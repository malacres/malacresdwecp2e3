# README #

Este es el ejercicio 3 de la practica de Diseño Web en Entorno CLiente 2016-17 cuyo eneunciado es:

El objetivo es hacer una especie de tienda online y un carrito de compra (aunque sin su funcionalidad completa).
Para ello haremos una página. Al cargase la página:
• Si ya se cargo una tienda antes, la obtenga de los datos almacenados localmente, tanto de la tienda como del carrito de compra.
• Si no había una tienda antes, al cargar pida mediante AJAX una tienda online. La tienda online estará formada por un array de objetos con los siguientes atributos:
• Identificador
• Categoría
• Nombre articulo
• Unidades disponibles
• Precio
El array podrá estar desordenado.
La tienda online al recibirse se guardará en datos locales, para hacer que sea persistente. Además deberá mostrarse la tienda online en una tabla siguiendo las siguientes especificaciones:
• Deberán mostrarse los artículos ordenados por categorías. Dentro de una misma categoría el orden dará igual.
• Cada articulo tendrá un input de tipo “text” y un botón “Añadir al carrito”.
• Ese botón añadirá al carrito de la compra los elementos que indique el input de tipo “text”. Antes de añadir se comprobará que el campo es numérico y que haya suficientes existencias disponibles del objeto solicitado (osea, que las solicitadas más las ya presentes en el carrito sean menores o iguales que las totales).
• Cuando se añada algo al carrito, además de mostrarse como se indica mas abajo, se almacenará en datos locales.
Además habrá otra tabla para representar el array de compras del carrito.
El carrito de la compra estará formado por array de objetos como el de la tienda, solo que unidades indicará cuantas unidades del elemento están en el carrito.
Al final de la página existirán los siguientes botones:
• Un botón “Vaciar carrito” que vaciará el carrito tanto a nivel visual como en datos locales.
• Un botón “Recargar tienda” que volverá a cargar la tienda de nuevo mediante una petición AJAX, vaciando el carrito tanto visualmente como en datos locales. La antigua tienda se perderá y se guardará la nueva recibida en datos locales.