//Manuel Lage Crespo DWEC 2016-2017
//JS del Ejercicio 3 de la practica de la segunda Evaluacion
//El ejercico esta resuelto segun el enunciado de la practica

//funcion que obtiene la tienda onlina almacenada a traves de localStore y la convierte con JSon;
function creaArrayTienda(){
     var arraytiendaJson=localStorage.getItem("arraytiendastore");
    var arraytienda=JSON.parse(arraytiendaJson);
    return arraytienda;
}
//funcion que obtiene el carro almacenado a traves de localStore y la convierte con JSon
function creaArrayCarro(){
    var arraycarroJson=localStorage.getItem("arraycarrostore");
    var arraycarro=JSON.parse(arraycarroJson); 
    return arraycarro;
}
//Busca un articulo segun el identificador dentro del array de la tienda almacenada y devuelve el objeto coincidente
function buscaArticuloTienda(id){
   var arraytienda=creaArrayTienda();
    for (var i=0;i<arraytienda.length;i++){
        if (arraytienda[i].id==id){
            return arraytienda[i];
        }
    }
}
//funcion que busca un articulo dentro del carrito si y si lo encuentra devuelve la posicion dentro del array y si no lo encuentra devuelve -1
function buscaArticuloCarro(id){
    var arraycarro=creaArrayCarro(); 
    for (var i=0;i<arraycarro.length;i++){
        if (arraycarro[i].id==id){
            return i;
        }
    }
    return -1;
}

//Funcion que borra el fichero del carrito y vacia la tabla de carro
function borraCarro(){
     $("#carro").empty();
    localStorage.removeItem("arraycarrostore");
}

//funcion que borra la tiendaonline almacenada, la tabla y tambien el carrito
function borraTienda(){
    localStorage.removeItem("arraytiendastore");
    borraCarro();
    cargaInical();
    
}

//Funcion que crea una celda con el contenido que le pasamos
function creaCelda(param){
    return("<td>"+param+"</td>");
}

//Funcion que obtiene el la cantidad de productos que queremos añadir al carrito y luego lo deja en blanco
function obtieneText(i){
    var text="#text"+i;
    var cantidad=$(text).val();
    $(text).val("");
    return parseInt(cantidad);
}

//funcion que carga el carro de la compra en una tabla eliminando la que hay
function cargaCarro(){
    var arraycarro=creaArrayCarro();
    //Si el carro SI tiene elementos...
    if(arraycarro!=null){
    $("#carro").empty();
    $("#carro").append("<tr><td>Productos de carro</td><td>Unidades</td><td>Precio</td></tr>"); 
    //recorre todo el arraycarro para ir insertando elementos en la tabla
        for(var i=0;i<arraycarro.length;i++){
        var nom=arraycarro[i].nombre;
        var unidades=arraycarro[i].unidades;
        var precio=arraycarro[i].precio;
       $("#carro").append("<tr>"+creaCelda(nom)+creaCelda(unidades)+creaCelda(precio)+"</tr>");
        }
    }
}

//Esta funcion es quizas la que tiene un mayor peso en el codigo, se ejecuta a traves de anade() y se encarga de gestionar la logica del programa, es decir, coge el valor del campo text en el momento de la pulsacion y matiene al dia el carrito de la compra

function controlCarro(articulo,cantidad){
var arraycarro=creaArrayCarro();
            /*Aqui compruebo tres posibles circunstancias:
                1.- que todiavia no hubiese elementos en el carrito, con lo que se crearia el array,  se cambiara el valor de las unidades y se añade al arraycarro.
                2.- que si hubiese array pero que no estuviese ese producto, con lo que modifico la cantidad del articulo buscado para añadirlo al array
                3.-que si hubiese el producto dentro del carrito, con podria actualizar el carrito o incluso eleminar un objeto del carrito si primero le añado 2 y luego le quito 2 ("-2")*/
            
            if (arraycarro==null){
                //alert("no habia carrito");
                arraycarro=new Array();
                articulo.unidades=parseInt(cantidad);
                arraycarro.push(articulo);
                localStorage.setItem("arraycarrostore",JSON.stringify(arraycarro));
                }
            else{
                //alert(articulo.id);
                var busqueda=buscaArticuloCarro(articulo.id);
                //alert(busqueda);
                if (busqueda==-1){
                    //alert("no habia antes");
                    articulo.unidades=parseInt(cantidad);
                    arraycarro.push(articulo);
                }else{
                    //alert("si habia antes");
                    var resultado=parseInt(arraycarro[busqueda].unidades)+parseInt(cantidad);
                    if ((resultado>0) && (resultado<=articulo.unidades)){
                        //alert(busqueda);
                        arraycarro[busqueda].unidades=resultado;
                    }else{
                        if (resultado==0){
                            //alert("eliminar articulo");
                            //alert(busqueda);
                            arraycarro.splice(busqueda,1);
                        }else{
                            alert("no podes tener en el carro una cantidad negativa del producto o la suma de tus productos sobrepasa el total del stock");
                            }
                        }
                }
            }
            //Por ultimo alamaceno el carrito y lo cargo de nuevo
        localStorage.setItem("arraycarrostore",JSON.stringify(arraycarro));
        cargaCarro();
}
//Esta funcion  coge el valor del campo text en el momento de la pulsacion, lo valida y si todo es correcto lo envia a controlCarro


function anade(id){
    
    var text= obtieneText(id);
    //Si el text SI es un numero...
    if (!isNaN(text)){
        var articulo=buscaArticuloTienda(id); 
        //Si existen suficiente existencias...
        if (parseInt(text)<=articulo.unidades){
            controlCarro(articulo,text);
        }
        else{   
        alert("No puedes comprar tantos");
        }
    }else{
        alert("comprueba las unidades que quieres compra");
    }        
}
    

//Esta funcion se encarga de carga el contenido de la itenda online en la pagina web  mediante una tabla.
function procesaCarga(tienda) {
    //ordena los elementos segun su categoria
    tienda.sort(function (a,b){
       return((a.cat)-(b.cat))});
    $("#tabla").empty();
    $("#tabla").append("<tr><td>Identificador</td><td>Categoria</td><td>Nombre</td><td>Unidades</td><td>Precio</td></tr>");   
    for(var i=0;i<tienda.length;i++){
        var id=tienda[i].id;
        var cat=tienda[i].cat;
        var nom=tienda[i].nombre;
        var unidades=tienda[i].unidades;
        var precio=tienda[i].precio;
        var text="<input type='text' id='text"+id+"'/>";
        var button="<input type='button' value='Anadir' onclick='anade("+id+")'/>";
       $("#tabla").append("<tr>"+creaCelda(id)+creaCelda(cat)+creaCelda(nom)+creaCelda(unidades)+creaCelda(precio)+creaCelda(text)+creaCelda(button)+"</tr>");   
        }
    }

//Procesamos lo que obtenemos de la respuesta de la llamada al servidor,
    //almacenamos lo que obtenelos en localStore
    //procesamos la carga de la tienda en la pagina
function procesaDatos(obj){
    localStorage.setItem("arraytiendastore",JSON.stringify(obj));
    procesaCarga(obj);
}


// Hacemos la llamada al servidor y procesamos la resupuesta
function obtenerDisponibilidad()
{
    //variables $ajax
    var url = "http://hispabyte.net/DWEC/entregable2-3.php";
    var metodo = "POST";
    var tipo_dato="JSON";
    
    //llamada jquery de AJAX
    
    $.ajax({method:metodo,url:url,dataType:tipo_dato})
        .done(function(msg){
        procesaDatos(msg);
    });
}

//Funcion que se ejecuta cuando se carga la pagina, comprueba si hay o no una tiendaonline guardada, si no la hay la obitene del servidor y si ya la hay carga la tienda y el carrito
function cargaInical() {
         var arraytienda=creaArrayTienda();
        if (arraytienda==null){
            obtenerDisponibilidad();
        }else{
            procesaCarga(arraytienda);
            cargaCarro();
        }
}  

// Asignamos lo que quermos haga cuando le damos al boton vaciar y cargar
window.onload = function() {
    cargaInical();
    $("#vaciar").click(borraCarro);
    $("#cargar").click(borraTienda);
}
